/*global contract, config, it, assert*/
const Portfolio = require('Embark/contracts/Portfolio');

let accounts;

// For documentation please see https://embark.status.im/docs/contracts_testing.html
config({
  //deployment: {
  //  accounts: [
  //    // you can configure custom accounts with a custom balance
  //    // see https://embark.status.im/docs/contracts_testing.html#Configuring-accounts
  //  ]
  //},
  contracts: {
    "Portfolio": {
      args: []
    }
  }
}, (_err, web3_accounts) => {
  accounts = web3_accounts
});

contract("Portfolio", function () {
  this.timeout(0);

  it("should set constructor owner", async function () {
    let result = await Portfolio.methods.owner().call();
    assert.strictEqual(result, accounts[0]);
  });

  it("set firstName value", async function () {
    await Portfolio.methods.updateFirstname('first').send();
    let result = await Portfolio.methods.firstName().call();
    assert.strictEqual(result, 'first');
  });

  it("set lastName value", async function () {
    await Portfolio.methods.updateLastname('last').send();
    let result = await Portfolio.methods.lastName().call();
    assert.strictEqual(result, 'last');
  });

  it("get full name values", async function () {
    let result = await Portfolio.methods.getFullname().call();
    assert.strictEqual(result._firstName, 'first');
    assert.strictEqual(result._lastName, 'last');
  });

  it("increment skill value", async function () {
    await Portfolio.methods.incrementSkills().send();
    let result = await Portfolio.methods.skillsTotal().call();
    assert.ok(parseInt(result, 10) > 0);
  });

  it("set hire status value", async function () {
    await Portfolio.methods.updateHireStatus(true).send();
    let result = await Portfolio.methods.hireMe().call();
    assert.strictEqual(result, true);
  });
})
